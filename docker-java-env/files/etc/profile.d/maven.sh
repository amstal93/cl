#!/bin/bash
# Apache Maven Environmental Variables
# MAVEN_HOME for Maven 1 - M2_HOME for Maven 2
export M2_HOME=/opt/maven/apache-maven-3.6.1
export MAVEN_HOME=/opt/maven/apache-maven-3.6.1
export PATH=${M2_HOME}/bin:${PATH}
